//
//  tweet.hpp
//  example-search
//
//  Created by Joao Freire on 8/29/16.
//
//

#ifndef tweet_hpp
#define tweet_hpp

#include "ofMain.h"

class tweet_entry
{
public:
    void create(string _author, string _text);
    void draw(int offsetx);
    string wrapString(string text, int width);
    
private:
    string text;
    string author;
    ofImage image;
    ofTrueTypeFont verdana;
    
    
};
#endif /* tweet_hpp */
