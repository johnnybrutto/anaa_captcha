#include "testApp.h"
ofTrueTypeFont font;

//--------------------------------------------------------------
void testApp::setup(){
    
    ofBackground(255);
    ofSetFrameRate(60);
    
    string const CONSUMER_KEY = "sT6wTIXLsMDmRnQoD6M9d1Vzk";
    string const CONSUMER_SECRET = "U7HST2sNIOheMbPRMiIuOfrVBA0G33t8ogCCwSy7PR5ZHRG470";
    
    twitterClient.authorize(CONSUMER_KEY, CONSUMER_SECRET);
    
    actualTweet = 0;
    newTweetSearch = false;
    num_tweets = 0;
    lastTime = 0;
    currTime = 0;
    // search();
    monoLineTextInput.setup();
    monoLineTextInput.text = "";
    monoLineTextInput.bounds.x = 510;
    monoLineTextInput.bounds.y = 430;
    monoLineTextInput.bounds.height = 60;
    monoLineTextInput.bounds.width = 545;
    font.loadFont(OF_TTF_SERIF, 18);
    monoLineTextInput.setFont(font);
    monoLineTextInput.multiline = true;
    
    _iswriting = false;
    ntests = 0;
    ofSetCircleResolution(250);
    //ofSetVerticalSync(true);
    ofEnableAlphaBlending();
    monoLineTextInput.drawCursor = true;
    
    /*font12.loadFont("HelveticaNeue.ttf",12, true, true, 500);
    font22.loadFont("HelveticaNeue.ttf",22, true, true, 500);
    font20.loadFont("HelveticaNeue.ttf",20, true, true, 500);
    font14.loadFont("HelveticaNeue.ttf",14, true, true, 500);
    font26.loadFont("HelveticaNeue.ttf",15, true, true, 500);
    */
    
    font12.loadFont("Helvetica.ttf",12);
    font22.loadFont("Helvetica-Bold.ttf",20);
    fontsmallbold.loadFont("Helvetica-Bold.ttf",12);

    fontTweet.loadFont("Helvetica-Bold.ttf",15);

    font22.setSpaceSize(0.5);

    font20.loadFont("Helvetica.ttf",20);
    font14.loadFont("Helvetica.ttf",14);
    font26.loadFont("Helvetica.ttf",15);
    


    monoLineTextInput.setFont(font26);
    tweetButton.loadImage("Botao_TweetCaptchat.png");
    tweetButton.setAnchorPercent(0, 0);
    crossButton.loadImage("x_TweetCaptchat.png");
    crossButtonarea.set(440 + 620 - crossButton.width - 10, 425 + 5, crossButton.width, crossButton.height);
    sendtweetButtonarea.set(440 + 620 - tweetButton.width, 425 + 70 + 70 - tweetButton.height, tweetButton.width, tweetButton.height);
    starttweetButtonarea.set(ofGetWidth() - 100 - tweetButton.width , 40 - tweetButton.getHeight()/2, tweetButton.width, tweetButton.height);

    logo.loadImage("chameleon_branco.png");
    _changeColor = false;
    _Colorup = false;
    logoColor.set(0, 132, 180);
    startColor.set(0, 59, 102);
    endColor.set(0, 132, 180);
    
    search();
    

}

//--------------------------------------------------------------
void testApp::update()
{
    
     if(ofGetElapsedTimef() - currTime > 10)
     {
         currTime = ofGetElapsedTimef();
         search();
     }
    
    // cout<<ofGetElapsedTimef() - lastTime<<endl;
    if(ofGetElapsedTimef() - lastTime > 20)
    {
        
        lastTime = ofGetElapsedTimef();
        //   twitterClient.postStatus("leave it running #johnnypersonalhashtag2 " + ofToString(ntests));
        ntests++;
        
    }
    
    /*if(newTweetSearch)
    {
        newTweetSearch = false;
        if(twitterClient.getTotalLoadedTweets() > num_tweets)
        {
            for(int i = 0; i < twitterClient.getTotalLoadedTweets() - num_tweets; i++)
            {
                tweet = twitterClient.getTweetByIndex(i);
                tweet_entry aux;
                aux.create(tweet.user.screen_name, tweet.text);
                if(num_tweets == 0)
                    tweets.push_back(aux);
                else
                    tweets.insert(tweets.begin(), aux);
            }
            num_tweets = twitterClient.getTotalLoadedTweets();
        }
    }*/
    
    if(!_iswriting && monoLineTextInput.getIsEditing())
    {
        _iswriting = true;
        open(1000);
    }
    
    if(_iswriting && !monoLineTextInput.getIsEditing())
    {
        _iswriting = false;
        //if(monoLineTextInput.text != "")
        //    twitterClient.postStatus(monoLineTextInput.text);
        //monoLineTextInput.clear();
        close(1000);
    }
    
  
}

//--------------------------------------------------------------
void testApp::draw()
{
    ofSetBackgroundColor(255, 255, 255);
    drawCover(0);
    /*ofRect(monoLineTextInput.bounds);�
     ofNoFill();
     monoLineTextInput.draw();
     ofSetColor(255,0,0);
     ofCircle(ofGetMouseX(), ofGetMouseY(), 5);*/
    
}


void testApp::drawCover(int location)
{
    //BIG RECT
    ofPushMatrix();
    ofFill();
    ofSetColor(245, 248, 250);
    ofRect(0,0,ofGetWidth(), ofGetHeight());
    
    ofSetColor(255,255,255);
    ofRect(0,330, ofGetWidth(), 70);//MEDIUM BAR
    
    
    
    ofPushMatrix();//tweets bar
    ofFill();
    ofSetColor(255,255,255);
    ofRectRounded(ofRectangle(440,510,620,55), 5);
    ofSetColor(204, 214, 221);
    ofNoFill();
    ofRectRounded(ofRectangle(440,510,620,55), 5);
    ofPopMatrix();
    
    //DRAW TEXT
    ofPushMatrix();
    ofSetColor(41, 47, 51);
    
    font22.drawString("Captcha-T Lisboa", 100, 460);
    //font20.drawString("TWEETS", 450, 645);
     
      ofSetColor(102,117, 127);
    font14.drawString("@captchat_lisboa", 100, 485);
    font12.drawString("TWEETS", 440, 350);
    font20.drawString(ofToString(tweets.size()), 460, 380);
    ofPopMatrix();
    
    ofPushMatrix();
    for(int i = 0; i < tweets.size(); i++)
    {
        tweets[i].draw(i);
    }
    ofPopMatrix();
    
    if(_iswriting || pop.isRunning())
    {
        ofFill();
        ofPushMatrix();
        ofSetColor(100,100,100,pop.update()*100);
        ofRect(0,0,ofGetWidth(), ofGetHeight());
        
        ofSetColor(245, 248, 250,255);
        ofRectRounded(ofRectangle(440, 425,620, 70 + pop.update()*75), 5);
        ofPopMatrix();
        
        ofSetColor(255,255,255);
        
    }
    
    //text input draw
    ofPushMatrix();
    ofFill();
    ofSetColor(0, 132, 180);
    ofRect(0,70,ofGetWidth(),260);
    ofSetColor(255,255,255);
    ofRectRounded(ofRectangle(100-5,190-5,250,250), 10);//BIG LOGO
    ofSetColor(0, 59, 102);
    ofRectRounded(ofRectangle(100,190,240,240), 10);//BIG LOGO
    ofRectRounded(ofRectangle(445,430,60,60), 10);//SMALL LOGO
    ofRect(435,390, 80,10);
    ofPopMatrix();
    
    ofPushMatrix();
    string description = wrapString("CAPTCHA�T proposes a re-interpretation of Alan Turing�s Imitation Game. Can you tell who's tweeting from the other side: a human or a bot? Use this space to create and share ideas about the PLUNC festival.", 245);
    fontsmallbold.drawString(description, 100, 505);
    
    
    ofPushMatrix();
    ofNoFill();
    ofSetColor(204, 214, 221);
    ofSetLineWidth(1);
    ofRectRounded(ofRectangle(440, 425,620, 70 + pop.update()*75), 5);
    ofFill();
    ofSetColor(255,255,255);
    ofRect(monoLineTextInput.bounds.getTopLeft(), monoLineTextInput.bounds.getWidth(), monoLineTextInput.bounds.getHeight() +75*pop.update());
    ofSetColor(0, 0, 0);
    string new_text = wrapString(monoLineTextInput.text, 540);
    string old = monoLineTextInput.text;
    monoLineTextInput.text = new_text;
    if(monoLineTextInput.getIsEditing() &&  pop.update() == 1)
        monoLineTextInput.draw();
    
    monoLineTextInput.text = old;
    ofPopMatrix();
    
    if(pop.update() == 1)
    {
        ofSetColor(255,255,255);
        
        tweetButton.draw(440 + 620 - tweetButton.width , 425 + 70 + 70 - tweetButton.height);
        
        crossButton.draw(crossButtonarea);
       // ofSetColor(102,117, 127);
        //fontTweet.drawString("Tweet", 440 + 620 - tweetButton.width + 20 , 425 + 70 + 70 - tweetButton.height/2);
    }
    
    ofPushMatrix();
    ofSetColor(255,255,255);
    ofRect(0,0,ofGetWidth(), 70);//UPPER BAR
    ofPopMatrix();
    
    ofPushMatrix();
    
    if(ofGetElapsedTimef() - colorTime > 10)
    {
        colorTime = ofGetElapsedTimef();
        _changeColor = true;
        _Colorup = !_Colorup;
        colorTween.setParameters(7,easinglinear,ofxTween::easeOut,0,1,5000,0);
        colorTween.start();
    }
    int alphaStart;
    int alphaEnd;
    if(_Colorup)
    {
        alphaStart = 255 * colorTween.update();
        alphaEnd = 255 - alphaStart;
        
    }
    else
    {
        alphaEnd = 255 * colorTween.update();
        alphaStart = 255 - alphaEnd;
    }

    ofSetColor(startColor.r, startColor.g, startColor.b, alphaStart);
    logo.draw(ofGetWidth()/2 - logo.width/4  ,50-logo.height/4, logo.width/2, logo.height/2);


    ofSetColor(endColor.r, endColor.g, endColor.b, alphaEnd);
    logo.draw(ofGetWidth()/2 - logo.width/4  ,50-logo.height/4, logo.width/2, logo.height/2);
    ofPopMatrix();
    
    ofSetColor(0,0,0);
    fontsmallbold.drawString("ANAA", 100, 50);
    ofSetColor(255,255,255);
    tweetButton.draw(ofGetWidth() - 100 - tweetButton.width , 40 - tweetButton.getHeight()/2);
   
    
    ofFill();
    ofSetColor(255,0,0);
    ofCircle(ofGetMouseX(), ofGetMouseY(), 5);
    
    
}

void testApp::open(int duration)
{
    pop.setParameters(7,easinglinear,ofxTween::easeOut,0,1,duration,0);
    pop.start();
}


void testApp::close(int duration)
{
    pop.setParameters(7,easinglinear,ofxTween::easeOut,1,0,duration,0);
    pop.start();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
    //if(key == 's')
      //  search();
}

void testApp::search()
{
    
    cout<<"SEARCH"<<endl;
    std::string file = "../../python/tweets.json";

    // Now parse the JSON
    bool parsingSuccessful = result.open(file);
    int actualTweets = result.size();
    cout<<"I have "<<actualTweets<<endl;
    
    for(int i = num_tweets; i < actualTweets; i++)
    {
        tweet_entry aux;
        aux.create(result[ofToString(i)][1]["author"].asString(), result[ofToString(i)][0]["text"].asString());
        if(num_tweets == 0)
            tweets.push_back(aux);
        else
            tweets.insert(tweets.begin(), aux);
    }
    
    num_tweets = actualTweets;
}


string testApp::wrapString(string text, int width)
{
    string typeWrapped = "";
    string tempString = "";
    vector <string> words = ofSplitString(text, " ");
    
    for(int i=0; i<words.size(); i++)
    {
        string wrd = words[i];
        int stringwidth = font26.stringWidth(tempString + " " + wrd);
        if(stringwidth >= width)
        {
            tempString = wrd;
            typeWrapped += "\n";
        }
        else
            tempString += wrd + " ";
        
        typeWrapped += wrd + " ";
    }
    
    if(typeWrapped == "")
        typeWrapped = text;
    
    return typeWrapped;}

//--------------------------------------------------------------
void testApp::keyReleased(int key)
{
    if (key == OF_KEY_RETURN)
    {
        if(monoLineTextInput.getIsEditing() && monoLineTextInput.text != "")
        {
            monoLineTextInput.endEditing();
            monoLineTextInput.clear();
            twitterClient.postStatus(monoLineTextInput.text);
        }
    }
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
    cout<<"HERE"<<endl;
    
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    if(monoLineTextInput.getIsEditing() && monoLineTextInput.text != "" && sendtweetButtonarea.inside(ofGetMouseX(), ofGetMouseY()))
    {
        monoLineTextInput.endEditing();
        monoLineTextInput.clear();
        twitterClient.postStatus(monoLineTextInput.text);
    }
    
   
    
    
    if(!monoLineTextInput.getIsEditing() && starttweetButtonarea.inside(ofGetMouseX(), ofGetMouseY()))
    {
        monoLineTextInput.beginEditing();
    }
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    if(monoLineTextInput.getIsEditing()  && crossButtonarea.inside(ofGetMouseX(), ofGetMouseY()))
    {
        monoLineTextInput.endEditing();
    }
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 
    
}
