//
//  tweet.cpp
//  example-search
//
//  Created by Joao Freire on 8/29/16.
//
//

#include "tweet.hpp"

void tweet_entry::create(string _author, string _text)
{
    verdana.loadFont("HelveticaNeue.ttf",15, true, true, 500);
    author = _author;
    
    _text.erase(std::remove(_text.begin(), _text.end(), '\n'), _text.end());

    text = wrapString(_text, 540);
}

void tweet_entry::draw(int offsetx)
{
    ofPushMatrix();
    ofFill();
    ofSetColor(255,255,255);
    ofRect(440,560+ offsetx*100,620,100);
    ofNoFill();
    ofSetColor(204, 214, 221);
    ofRect(440,560 + offsetx*100,620,100);
    
    ofFill();
    ofSetColor(0, 59, 102);
    ofRectRounded(ofRectangle(445,565 + offsetx*100, 60,60), 10);
    ofSetColor(41,47,51);
    string comingfrom;
    if(author == "anaacolectivo")
        comingfrom = "Plunc Lisboa";
    else
        comingfrom = "Plunc Almada";
    
    verdana.drawString(comingfrom, 440 + 70, 580 + offsetx*100);
    ofSetColor(102,117,127);

    verdana.drawString(text, 440 + 70, 600 + offsetx*100);
}

string tweet_entry::wrapString(string text, int width) {
   // verdana.setLineHeight(20.0f);
    string typeWrapped = "";
    string tempString = "";
    vector <string> words = ofSplitString(text, " ");
    
    for(int i=0; i<words.size(); i++)
    {
        string wrd = words[i];
        int stringwidth = verdana.stringWidth(tempString + " " + wrd);
        if(stringwidth >= width)
        {
            tempString = wrd;
            typeWrapped += "\n";
        }
        else
            tempString += wrd + " ";

        typeWrapped += wrd + " ";  
    }  
    
    return typeWrapped;
}  
